/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "dac.h"
#include "dma.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */
int _write(int file, uint8_t* ptr, int len){
	HAL_UART_Transmit(&huart2,ptr,len,50);
	return len;
}

//////////////////////////////////////////////////////////////////
//UART
//////////////////////////////////////////////////////////////////
unsigned char Received[100];
volatile uint8_t recivedIter=0;
volatile uint8_t needToDoCommand=0;

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {
	if(Received[recivedIter]=='\r'){
		needToDoCommand=1;
	} else if(recivedIter>=99){
		needToDoCommand=1;
	} else {
		recivedIter++;
		HAL_UART_Receive_IT(&huart2, &Received[recivedIter], 1);
	}
}
//////////////////////////////////////////////////////////////////
//ADC
//////////////////////////////////////////////////////////////////
uint16_t ADC_value;
uint8_t adc_flag=0;

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef *hadc){
	//printf("ADC\r\n");
	ADC_value = HAL_ADC_GetValue(&hadc1);
	adc_flag=1;
}
//////////////////////////////////////////////////////////////////
//COMM
//////////////////////////////////////////////////////////////////
uint8_t signalType=3; //0-const, 1-sin,2-tri,3-rect
float signalAmpl = 1.0;
float signalOffs = 1.0;
float signalFreq = 1.0;

uint8_t ADC_showFlag=0;
uint16_t ADC_counter=0;

uint8_t eq(unsigned char *str1, const char* str2){
	for(uint8_t i=0; i< strlen(str2); i++){
		if(str1[i]!=str2[i]) return 0;
	}
	return 1;
}

float commandToFloat(unsigned char *command){ //command = "ampl=1.23"
	char paramValText[5];
	paramValText[0] = command[5];
	paramValText[1] = command[6];
	paramValText[2] = command[7];
	paramValText[3] = command[8];
	paramValText[4] = '\0';

	return atof(paramValText);
}

uint8_t commandToInt(unsigned char *command){ //command = "sig=1"
	return command[4]-'0';
}
//////////////////////////////////////////////////////////////////
//DAC
//////////////////////////////////////////////////////////////////
uint8_t signal[100];

void regenerateSignalTables(){
	for(int i=0; i<100; i++){
		int16_t tmp = 0;
		if(signalType==0){
			tmp=(int16_t)signalOffs/3.3*255.0;
		} else if(signalType==1){
			tmp = (int16_t)((signalAmpl*sin(2.0*M_PI*((float)i)/101.0) + signalOffs)/3.3*255.0);
		} else if(signalType==2){
			tmp = (int16_t)((-signalAmpl+signalOffs+2.0*signalAmpl*((float)i)/99.0)/3.3*255.0);
		} else if(signalType==3){
			if(i<50){ tmp =  (int16_t)((-signalAmpl+signalOffs)/3.3*255.0); }
			if(i>=50){ tmp = (int16_t)((signalAmpl+signalOffs)/3.3*255.0); }
		}

		if(tmp>255) tmp = 255;
		if(tmp<0) tmp = 0;
		signal[i] = (uint8_t)tmp;
	}
}

void resetSignal(){
	regenerateSignalTables();

	htim6.Init.Period = ((int16_t)(100.0/signalFreq)-1);
	  if (HAL_TIM_Base_Init(&htim6) != HAL_OK)
	  {
	    Error_Handler();
	  }
}
//////////////////////////////////////////////////////////////////
//ENGINE
//////////////////////////////////////////////////////////////////
void doCommand(){
	if(eq(Received,"help")){
		printf("	Commands: help, info, ampl=,freq=,offs=,sig=,adc,\r\n");
	} else if(eq(Received,"info")){
		printf("	Signal info:\r\n");
		printf("  	  Signal type -> %d\r\n",signalType);
		printf("  	  Amplitude -> %d.%.2d [V]\r\n",(int)signalAmpl,(int)(signalAmpl*100)%100);
		printf("  	  Frequency -> %d.%.2d [Hz]\r\n",(int)signalFreq,(int)(signalFreq*100)%100);
		printf("  	  Offset -> %d.%.2d [V]\r\n",(int)signalOffs,(int)(signalOffs*100)%100);
	} else if(eq(Received,"ampl=")){
		signalAmpl = commandToFloat(Received);
		resetSignal();
		printf("	Amplitude set to %d.%.2d [V]\r\n",(int)signalAmpl,(int)(signalAmpl*100)%100);
	} else if(eq(Received,"freq=")){
		signalFreq = commandToFloat(Received);
		resetSignal();
		printf("	Frequency set to %d.%.2d [Hz]\r\n",(int)signalFreq,(int)(signalFreq*100)%100);
	} else if(eq(Received,"offs=")){
		signalOffs = commandToFloat(Received);
		resetSignal();
		printf("	Offset set to %d.%.2d [V]\r\n",(int)signalOffs,(int)(signalOffs*100)%100);
	} else if(eq(Received,"sig=")){
		signalType = commandToInt(Received);
		resetSignal();
		printf("	Signal type set to %d\r\n",signalType);
	} else if(eq(Received,"adc")){
		ADC_showFlag=1;
		ADC_counter=0;
		printf("	ADC on\r\n");
	} else {
		printf("	Unknown command!\r\n");
	}

	needToDoCommand=0;
	recivedIter=0;
	HAL_UART_Receive_IT(&huart2, &Received[0], 1);
}
///////////////////////////////////////////////////////////////////////////////////
//TIMER
///////////////////////////////////////////////////////////////////////////////////
uint16_t timeAdcInMs;
uint8_t ledCounter=0;

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	if(htim=&htim7){
		timeAdcInMs += 10;
		ledCounter += 1;
		if(ledCounter>=50){
			HAL_GPIO_TogglePin (LD2_GPIO_Port , LD2_Pin ) ;
			ledCounter = 0;
		}
	}
}


/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_USART2_UART_Init();
  MX_DAC1_Init();
  MX_TIM6_Init();
  MX_ADC1_Init();
  MX_TIM7_Init();
  /* USER CODE BEGIN 2 */
  HAL_ADC_Start_IT(&hadc1);
  HAL_DAC_Start(&hdac1 , DAC_CHANNEL_1) ;

  HAL_UART_Receive_IT(&huart2, &Received[0], 1);
  HAL_TIM_Base_Start_IT(&htim7);

  HAL_DAC_SetValue(&hdac1 , DAC_CHANNEL_1, DAC_ALIGN_8B_R, 0 );

  resetSignal();

  HAL_DAC_Start_DMA(&hdac1, DAC_CHANNEL_1, &signal[0], 100 , DAC_ALIGN_8B_R);
  HAL_TIM_Base_Start(&htim6);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
	  if(ADC_showFlag==1){
		  HAL_ADC_Start_IT(&hadc1);
		  timeAdcInMs = 0;
		  for(uint16_t i=0; i<250; i++){
			  printf("	%d.%.2d %d.%.2d\r\n",timeAdcInMs/1000,(timeAdcInMs/10)%100,ADC_value*33/2550,ADC_value*33*100/2550%100);
			  HAL_ADC_Start_IT(&hadc1);
			  HAL_Delay(19);
		  }
		  ADC_showFlag=0;
	  }

	  if(needToDoCommand){
		  doCommand();
	  }
	  HAL_Delay(100);
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  RCC_OscInitStruct.PLL.PREDIV = RCC_PREDIV_DIV1;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART2|RCC_PERIPHCLK_ADC12;
  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  PeriphClkInit.Adc12ClockSelection = RCC_ADC12PLLCLK_DIV1;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
